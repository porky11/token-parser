#![deny(missing_docs)]

/*!
Some utilities for parsing some format based on nested lists into arbitrary data structures.
It's also meant to be used as a backend for parsers.
**/

use thiserror::Error;

/// A trait required for all contexts being used for token parsing.
///
/// By default, only the empty tuple implements it.
/// It currently does not contain anything by default. It's just there to achieve compatibility with features and to allow more changes without breaking anything.
pub trait Context {
    #[cfg(feature = "radix-parsing")]
    #[inline]
    /// Specifies the radix if the feature radix parsing is enabled.
    fn radix(&self) -> u32 {
        10
    }
}

impl Context for () {}

/// The error type for token parsing.
#[derive(Debug, Error)]
pub enum Error {
    /// The sublist contains less elements than expected by a specified amount.
    #[error("Not enough elements: {0}")]
    NotEnoughElements(usize),

    /// The sublist contains more elements than expected by a specified amount.
    #[error("Too many elements: {0}")]
    TooManyElements(usize),

    /// No list is allowed in this context.
    #[error("List not allowed")]
    ListNotAllowed,

    /// No symbol is allowed in this context.
    #[error("Symbol not allowed")]
    SymbolNotAllowed,

    /// Error with string parsing.
    #[error("String parsing error")]
    StringParsing,

    /// Some specific element is invalid.
    #[error("Invalid element")]
    InvalidElement,
}

/// The result type for token parsing.
pub type Result<T> = std::result::Result<T, Error>;

/// Some unit, which represents an intermediate state.
pub enum Unit {
    /// The current unit is a single symbol.
    Symbol(Box<str>),
    /// The current unit is a parser, which can yield multiple units.
    Parser(Parser),
}

impl Unit {
    /// Returns the symbol, if applicable, as a result type.
    pub fn symbol(self) -> Result<Box<str>> {
        use Unit::*;
        match self {
            Symbol(name) => Ok(name),
            Parser(_) => Err(Error::ListNotAllowed),
        }
    }

    /// Returns the parser, if applicable, as a result type.
    pub fn parser(self) -> Result<Parser> {
        use Unit::*;
        match self {
            Symbol(_) => Err(Error::SymbolNotAllowed),
            Parser(parser) => Ok(parser),
        }
    }
}

/// This trait needs to be implemented for every struct which can be parsed using the token parser.
pub trait Parsable<C: Context>: Sized {
    /// When a symbol is found by the parser, this will be called.
    fn parse_symbol(_name: Box<str>, _context: &C) -> Result<Self> {
        Err(Error::SymbolNotAllowed)
    }

    /// When a subparser is found by the parser, this will be called.
    fn parse_list(_parser: &mut Parser, _context: &C) -> Result<Self> {
        Err(Error::ListNotAllowed)
    }
}

fn parse<C: Context, P: Parsable<C>>(unit: Unit, context: &C) -> Result<P> {
    use Unit::*;
    match unit {
        Symbol(name) => Parsable::parse_symbol(name, context),
        Parser(mut parser) => parser.parse_rest(context),
    }
}

impl<C: Context, T: Parsable<C>> Parsable<C> for Box<T> {
    fn parse_symbol(name: Box<str>, context: &C) -> Result<Self> {
        Ok(Self::new(Parsable::parse_symbol(name, context)?))
    }

    fn parse_list(parser: &mut Parser, context: &C) -> Result<Self> {
        Ok(Self::new(parser.parse_list(context)?))
    }
}

impl<C: Context, T: Parsable<C>> Parsable<C> for Vec<T> {
    fn parse_list(parser: &mut Parser, context: &C) -> Result<Self> {
        let Parser { form, count } = parser;
        let result = form
            .drain(..)
            .rev()
            .map(|unit| {
                *count += 1;
                parse(unit, context)
            })
            .collect();
        result
    }
}

impl<C: Context> Parsable<C> for String {
    fn parse_symbol(name: Box<str>, _context: &C) -> Result<Self> {
        Ok(name.into())
    }
}

impl<C: Context> Parsable<C> for Box<str> {
    fn parse_symbol(name: Box<str>, _context: &C) -> Result<Self> {
        Ok(name)
    }
}

#[macro_export]
/// Derives `Parsable` from symbol for types which implement `FromStr`.
macro_rules! derive_symbol_parsable {
    ($t:ty) => {
        impl<C: $crate::Context> $crate::Parsable<C> for $t {
            fn parse_symbol(name: Box<str>, _context: &C) -> $crate::Result<Self> {
                if let Ok(value) = name.parse() {
                    Ok(value)
                } else {
                    Err($crate::Error::StringParsing)
                }
            }
        }
    };
    ($t:ty, $($rest:ty),+) => {
        derive_symbol_parsable!($t);
        derive_symbol_parsable!($($rest),+);
    };
}

#[cfg(not(feature = "radix-parsing"))]
mod numbers;
derive_symbol_parsable!(bool);

/// The token parser to parse the units into wanted types.
pub struct Parser {
    form: Vec<Unit>,
    count: usize,
}

impl Parser {
    /// Creates a new parser from a list of objects.
    pub fn new<I: IntoIterator>(form: I) -> Self
    where
        I::Item: Into<Unit>,
    {
        let mut form: Vec<_> = form.into_iter().map(I::Item::into).collect();
        form.reverse();
        Self { form, count: 0 }
    }

    /// Tries to parse the next unit as the required type.
    pub fn parse_next<C: Context, T: Parsable<C>>(&mut self, context: &C) -> Result<T> {
        self.count += 1;
        if let Some(token) = self.form.pop() {
            parse(token, context)
        } else {
            Result::Err(Error::NotEnoughElements(self.count))
        }
    }

    /// Tries to parse the rest of the current list into the required type.
    /// If not every available token is used, this will be an error.
    pub fn parse_rest<C: Context, T: Parsable<C>>(&mut self, context: &C) -> Result<T> {
        let result = self.parse_list(context);
        let count = self.form.len();
        if count > 0 {
            self.form.clear();
            Err(Error::TooManyElements(count))
        } else {
            result
        }
    }

    /// Tries to parse as many tokens of the current list as needed into the required type.
    pub fn parse_list<C: Context, T: Parsable<C>>(&mut self, context: &C) -> Result<T> {
        Parsable::parse_list(self, context)
    }
}

impl Iterator for Parser {
    type Item = Result<Self>;

    fn next(&mut self) -> Option<Result<Self>> {
        self.count += 1;
        Some(self.form.pop()?.parser())
    }
}

#[cfg(feature = "radix-parsing")]
/// Contains utilities for radix parsing.
pub mod radix;
