use crate::derive_symbol_parsable;

derive_symbol_parsable!(i8, i16, i32, i64, i128);
derive_symbol_parsable!(u8, u16, u32, u64, u128);
derive_symbol_parsable!(f32, f64);
derive_symbol_parsable!(usize);
